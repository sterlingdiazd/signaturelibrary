package com.jvra.signature;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.FileNotFoundException;
import java.io.IOException;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import com.jvra.R;

public class SignatureActivity extends Activity implements PropertyChangeListener{


	public class Options
	{
		public static final String SAVE_IMAGE_SIGNATURE = "saveSignature";
		public static final String SAVED_IMAGE_POINTS   = "pointsSignature";
	}
	
	private LinearLayout signatureHolder;
	private Button save;
	private Button cancel;
	private Button clear;
	private Button accept;
	private Panel panel;	
	private SignatureSerialization serialization;	
	private EventHandler handler;
	private boolean msgSaveSignature;	
	private ResponseSignature response;
	
	
	@Override
	public void onCreate( Bundle saveInstance )
	{
		super.onCreate( saveInstance );
		requestWindowFeature( Window.FEATURE_NO_TITLE );
		
		setContentView( R.layout.signature );
		
		initComponents();
	}
	
	private class EventHandler implements OnClickListener
	{

		public void onClick(View v) {
			
			if( v.getId() == cancel.getId() )
			{
				save.setEnabled( false );
				Bundle bundle = new Bundle();
				bundle.putString("status", "cancel");
				Intent intent = new Intent();
				intent.putExtras(bundle);
				setResult(RESULT_CANCELED, intent);
				finish();
			}
			if( v.getId() == clear.getId() )
			{
				panel.clear();
				save.setEnabled(false);
				return;
			}
			if( v.getId() == save.getId() )
			{
				
				Bundle bundle = new Bundle();				
				Intent intent = new Intent();								
				
				try {
					if( msgSaveSignature )
					{
						if( !serialization.save("Signature") )
						{
							bundle.putString("status", "cancel");
							intent.putExtras(bundle);
							setResult(RESULT_CANCELED, intent);							
						}
						
//						response.setBitmap( serialization.getBitmapSignature() );
//					    response.setPoints( panel.getPoints() );				
//					    response.setView( panel );
//						bundle.putSerializable( Options.SAVED_IMAGE_POINTS, response );						
					}
					bundle.putString("status", "done");
					intent.putExtras(bundle);
					setResult(RESULT_OK, intent);						
					
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
				finish();				
			}
			
			if( v.getId() ==R.id.accept ){
				setResult(RESULT_OK);
				finish();
			}
			
		}
		
	}
	
	
	private void initComponents()
	{
		try
		{
			Bundle bundle = getIntent().getExtras();
			if( bundle.getBoolean(Options.SAVE_IMAGE_SIGNATURE) )
				msgSaveSignature = true;
			
		}catch( Exception e )
		{
			msgSaveSignature = false;
			e.printStackTrace();
		}
		handler = new EventHandler();
				
		response = new ResponseSignature();
		
		signatureHolder = ( LinearLayout )findViewById( R.id.signatureContent );
		
		serialization = new SignatureSerialization(this,signatureHolder);
		
		save = ( Button )findViewById( R.id.save );
		save.setEnabled(false);
		
		cancel = ( Button ) findViewById( R.id.cancel );
		clear = ( Button ) findViewById( R.id.clear );
			
		accept = ( Button) findViewById( R.id.accept );
		
		panel = new Panel(this,null);
		panel.addPropertyChangeListener(this);
		panel.setBackgroundColor(Color.WHITE);
		
		signatureHolder.addView(panel, LayoutParams.FILL_PARENT,LayoutParams.FILL_PARENT);
		
		save.setOnClickListener( handler );
		cancel.setOnClickListener(handler);
		clear.setOnClickListener(handler);
		accept.setOnClickListener( handler );
			
	}
	
	public void propertyChange(PropertyChangeEvent event) {

			if( event.getPropertyName().equals("Signature") )
				save.setEnabled(true);
		
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
	
	
}
