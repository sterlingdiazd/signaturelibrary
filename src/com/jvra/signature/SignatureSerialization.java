package com.jvra.signature;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Canvas;
import android.os.Environment;
import android.provider.MediaStore.Images;
import android.view.View;


public class SignatureSerialization {

	private static final CompressFormat DEFAULT_COMPRESS_FORMAT = Bitmap.CompressFormat.PNG;
	private View signatureHolder;
	private String urlSavedImage;
	private Context context;
	private static final String directory = "Signature";
	private Bitmap bitmapSignature;
	
	public SignatureSerialization()
	{
		
	}
	public SignatureSerialization( Context context,View signatureHolder )
	{
		this.signatureHolder = signatureHolder;
		this.context = context;
		urlSavedImage = "";	
	}
	
	public boolean save( String signatureName ) throws IOException,FileNotFoundException,Exception
	{
		return save( signatureName,DEFAULT_COMPRESS_FORMAT );		
	}
	private boolean save( String signatureName,CompressFormat format ) throws Exception
	{
		if( context !=null )
		{
			
		String _filename = signatureName;
		
		if( isEnabledSDCard() )
		{
			if( _filename.equals("") )
				_filename = getUniqueName();
			
			if( resolveDirectory() )
			{
				
				ContextWrapper wraper = new ContextWrapper(context);
				File root = wraper.getDir(directory, Context.MODE_PRIVATE );
				
				_filename += getExtension(format);
				
				File signature = new File(root, _filename);
				
				if( bitmapSignature == null )
					bitmapSignature = Bitmap.createBitmap(signatureHolder.getWidth(), signatureHolder.getHeight(), Bitmap.Config.RGB_565);
				
				Canvas canvas = new Canvas( bitmapSignature );
				signatureHolder.draw(canvas);
				
				FileOutputStream out = new FileOutputStream(signature);
				
				bitmapSignature.compress(format, 90, out);
				out.flush();
				out.close();
				
				urlSavedImage = Images.Media.insertImage(context.getContentResolver(), bitmapSignature, "Signature", "Signature");
				return true;
				
			}else
				return false;
		}else
			throw new Exception("The SDCard is not mounted or Writable Access is not Permitted"); 
		}else
			throw new Exception( "The Context Signature is Null" );
		
	}
	public boolean savePNG( String signatureName ) throws IOException,FileNotFoundException,Exception
	{
		return save( signatureName,CompressFormat.PNG );
	}
	public boolean saveJPEG( String signatureName ) throws IOException,FileNotFoundException,Exception
	{
		return save( signatureName,CompressFormat.JPEG );
	}
	
	public Bitmap loadFile( String filename )
	{	
		return null;
	}
	public String getUrlSaved()
	{
		return urlSavedImage;
	}
		
	private boolean isEnabledSDCard()
	{
		
		boolean writableAccess = false;
		boolean enabled        = false;
		
		String status  = Environment.getExternalStorageState();
		
		if( status.equals( Environment.MEDIA_MOUNTED ) )
		{
			writableAccess = true;
			enabled = true;
		}else if( status.equals( Environment.MEDIA_MOUNTED_READ_ONLY ) )
		{
			writableAccess = false;
			enabled = true;
		}
		else
		{
			writableAccess = false;
			enabled = false;
		}
		return ( writableAccess && enabled );
	}
	
	private boolean resolveDirectory()
	{
		String tempDir = Environment.getExternalStorageDirectory()+"/"+directory;
		File file = new File(tempDir);
		
		if( !file.exists() )
			file.mkdirs();
		
		return file.isDirectory();
	}	
	
	private String getUniqueName()
	{
		Calendar c = Calendar.getInstance();
		int date = (c.get(Calendar.YEAR)*10000)+(c.get(Calendar.MONTH)*100)+(c.get(Calendar.DAY_OF_MONTH));
		int time = (c.get(Calendar.HOUR)*10000)+(c.get(Calendar.MINUTE)*100)+(c.get(Calendar.SECOND));
		
		return String.valueOf(date+"_"+time);
	}
	
	private String getExtension( CompressFormat format )
	{
		if( format == CompressFormat.JPEG )
			return ".jpeg";
		else if( format == CompressFormat.PNG )
			return ".png";
		else
			return "";
	}
	public View getSignatureHolder() {
		return signatureHolder;
	}
	public void setSignatureHolder(View signatureHolder) {
		this.signatureHolder = signatureHolder;
	}
	public String getUrlSavedImage() {
		return urlSavedImage;
	}
	public void setUrlSavedImage(String urlSavedImage) {
		this.urlSavedImage = urlSavedImage;
	}
	public Context getContext() {
		return context;
	}
	public void setContext(Context context) {
		this.context = context;
	}
	public Bitmap getBitmapSignature() {
		return bitmapSignature;
	}
	public void setBitmapSignature(Bitmap bitmapSignature) {
		this.bitmapSignature = bitmapSignature;
	}
	public static String getDirectory() {
		return directory;
	}
	
	
}
