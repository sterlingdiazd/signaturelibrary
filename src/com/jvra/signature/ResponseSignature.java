package com.jvra.signature;

import java.io.Serializable;
import java.util.LinkedList;

import android.graphics.Bitmap;
import android.graphics.Point;
import android.view.View;

public class ResponseSignature implements Serializable {

	private LinkedList< Point >points;
	private Bitmap bitmap;
	private View view;
	

	public ResponseSignature()
	{	
		
	}
	
	public ResponseSignature(LinkedList<Point> points) {
		super();
		this.points = points;
	}

	public LinkedList<Point> getPoints() {
		return points;
	}

	public void setPoints(LinkedList<Point> points) {
		this.points = points;
	}

	public Bitmap getBitmap() {
		return bitmap;
	}

	public void setBitmap(Bitmap bitmap) {
		this.bitmap = bitmap;
	}

	public View getView() {
		return view;
	}

	public void setView(View view) {
		this.view = view;
	}
	
	
}
