package com.jvra.signature;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.LinkedList;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

//Class for paint the signature

public class Panel extends View {

	private Path  path;
	private Paint paint;	
	private static final float STROKE_WIDTH = 2f;
	private PropertyChangeSupport changeSupport;
	private LinkedList< Point >points;
	
	
	public Panel(Context context) {
		super(context);
		initComponents();
	}

	public Panel(Context context, AttributeSet attrs) {
		super(context, attrs);
		initComponents();
	}

	public Panel(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initComponents();
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		canvas.drawPath(path, paint);
	}

	public Path getPath() {
		return path;
	}

	public void setPath(Path path) {
		this.path = path;
	}

	public Paint getPaint() {
		return paint;
	}

	public void setPaint(Paint paint) {
		this.paint = paint;
	}
	
	public LinkedList<Point> getPoints() {
		return points;
	}

	public void setPoints(LinkedList<Point> points) {
		this.points = points;
	}

	public PropertyChangeSupport getChangeSupport() {
		return changeSupport;
	}

	public void setChangeSupport(PropertyChangeSupport changeSupport) {
		this.changeSupport = changeSupport;
	}



	//clean the paint area and points holder
	public void clear()
	{
		path.reset();
		points.clear();
		invalidate();
	}
	
	private void initComponents()
	{
		path = new Path();
		paint = new Paint();
		
		//make this view observer og any listener
		changeSupport = new PropertyChangeSupport(this);
		points = new LinkedList<Point>();
		
		configurePaint();
	}
	
	//Adding listener to view. for any change
	public void addPropertyChangeListener( PropertyChangeListener listener )
	{		
		if( changeSupport !=null )
			changeSupport.addPropertyChangeListener("Signature", listener);			
	}
	
	private void configurePaint()
	{
		if( paint !=null )
		{
			paint.setAntiAlias( true );
			paint.setStrokeWidth( STROKE_WIDTH );
			paint.setColor( Color.BLACK );
			paint.setStrokeJoin( Paint.Join.ROUND );
			paint.setStyle(Paint.Style.STROKE );
		}		
	}

	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		 super.onTouchEvent(event);
		 		 		
		 changeSupport.firePropertyChange("Signature",false, true);
		 
		 float x = event.getX();
		 float y = event.getY();
		 
		 points.add( new Point((int)x, (int)y));
		 
		 switch( event.getAction() )
		 {
		 case MotionEvent.ACTION_DOWN :
		 {	
			 //move to path touch to down 
			path.moveTo(x, y);
			return true;
		 }
		 case MotionEvent.ACTION_UP : 
		 case MotionEvent.ACTION_MOVE:
		 {
			 //invalidate view and redrawing with the historical of touch event
			 for( int i=0;i<event.getHistorySize();++i )
			 {
				 float _x = event.getHistoricalX( i );
				 float _y = event.getHistoricalY( i );
				 path.lineTo(_x, _y);
			 }
			 path.lineTo(x, y);			 
		 }default:
			 break;
		 }
		 
		 
		 invalidate();
		 return true;
	}

	
}
